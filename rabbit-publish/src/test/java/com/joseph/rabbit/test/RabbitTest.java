package com.joseph.rabbit.test;

import com.joseph.rabbit.RabbitApplicationTests;
import com.joseph.rabbit.entity.Student;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.core.ReturnedMessage;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.AbstractJavaTypeMapper;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

public class RabbitTest extends RabbitApplicationTests {

    @Autowired
    RabbitTemplate rabbitTemplate;


    @Test
    public void publishTest() {

        for (int i = 0; i < 2; i++) {
            final int age = i;
            System.out.println("i===" + i);
            new Thread(() -> {
                for (int j = 0; j < 10; j++) {
                    rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
                    rabbitTemplate.setExchange("exchange.topic.cic");
                    rabbitTemplate.setRoutingKey("rounting.hfdks.cic");
                    Student student = new Student(j, Thread.currentThread().getName());
                    rabbitTemplate.convertAndSend(student, (Message msg) -> {
                        MessageProperties messageProperties = msg.getMessageProperties();
                        messageProperties.setDeliveryMode(MessageDeliveryMode.PERSISTENT);
                        messageProperties.setHeader(AbstractJavaTypeMapper.DEFAULT_CONTENT_CLASSID_FIELD_NAME, Student.class);
                        return msg;
                    });
                }
            }).run();
        }


    }

    @Test
    public void fanoutTest() {
        for (int i = 0; i < 1; i++) {

            rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
            rabbitTemplate.setExchange("exchange.topic.cic");
            rabbitTemplate.setRoutingKey("rounting.hfdks.cicc");
            // 保证消息发送到交换机
            rabbitTemplate.setConfirmCallback(new RabbitTemplate.ConfirmCallback() {

                @Override
                public void confirm(CorrelationData correlationData, boolean b, String s) {
                    System.out.println("ConfirmCallback=" + b);
                    System.out.println("ConfirmCallback=" + s);
                    System.out.println("ConfirmCallback=" + correlationData);
                }
            });
            // 保证消息发送到队列
            rabbitTemplate.setReturnsCallback(new RabbitTemplate.ReturnsCallback() {
                @Override
                public void returnedMessage(ReturnedMessage returnedMessage) {
                    System.out.println("returnedMessage=" + returnedMessage);
                }
            });
            Student student = new Student(i, Thread.currentThread().getName());
            rabbitTemplate.convertAndSend(student, (Message msg) -> {
                MessageProperties messageProperties = msg.getMessageProperties();
                messageProperties.setDeliveryMode(MessageDeliveryMode.PERSISTENT);
                messageProperties.setHeader(AbstractJavaTypeMapper.DEFAULT_CONTENT_CLASSID_FIELD_NAME, Student.class);
                return msg;
            });
        }

        // rabbitTemplate.convertAndSend("exchange.topic.cic", "rounting.hfdks.cic", student);
    }
}
