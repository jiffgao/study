package com.joseph;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DroolsLearnApplication {

    public static void main(String[] args) {
        SpringApplication.run(DroolsLearnApplication.class, args);
    }

}
