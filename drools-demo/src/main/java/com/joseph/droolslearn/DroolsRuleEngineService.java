package com.joseph.droolslearn;

import org.kie.api.KieBase;
import org.kie.api.runtime.KieSession;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author:
 * @createTime: 2023年05月08日 15:44:04
 * @version:
 * @Description:
 */
@Service
public class DroolsRuleEngineService {

    /**
     * 编译规则脚本，并执行规则
     * @param droolStrs
     * @param ruleExecutionObject
     * @param groupId
     * @return
     */
    public void compileRuleAndexEcuteRuleEngine(String droolStrs, Long groupId, Object o) {
        KieBase kieBase = null;
        try {
            // 编译规则脚本,返回KieSession对象
            // System.out.println(Class.forName("com.bjklb.engine.bean.AiLakeEntity"));
            kieBase = DroolsUtil.getInstance().getDrlSession(droolStrs, groupId);
            System.out.println("编译规则完成...");
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 执行规则
        executeRuleEngine(kieBase, o);
    }

    private void executeRuleEngine(KieBase kieBase, Object o) {
        KieSession kieSession = null;
        try {

            kieSession = kieBase.newKieSession();
            // for (Object o : factObjectList) {
            // logger.info("插入Fact对象：{}", o.getClass().getName());
            kieSession.insert(o);
            // }
            kieSession.fireAllRules();
            // logger.info("执行规则完成...");

            // return ruleExecutionObject;
        } catch (Exception e) {
            // logger.error("规则引擎执行出错:{}", e);
            throw new RuntimeException("ExceptionType.RULE_IS_ERROR");
        } finally {
            if (kieSession != null) {
                kieSession.dispose();
            }
        }
    }

}
