package com.joseph.droolslearn;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class AiLakeEntity {
    private double score;
    private String grade;
}
