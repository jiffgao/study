package com.joseph.droolslearn;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 0-通过
 * 1-拒绝
 * 2-转人工
 */
@Data
@Accessors(chain = true)
public class AirRiskEntity {
    /*人行征信无有效贷款记录*/
    private int noLoan;
    /*人行征信无有效贷记卡记录*/
    private int noCreditCard;
    /*人行征信无有效信用记录*/
    private int noValidCredit;
    /*人行征信中出现资产配置信息*/
    private int assetAllocation;
    /*人行征信中出现保证人代偿*/
    private int guarantorIndemnification;
    /*人行征信中出现呆账*/
    private int badDebt;
    /*人行征信近5年内逾期月份数大于等于10个月*/
    private int overdueMoreThanTenMonth;
    /*贷款或贷记卡出现过90天以上的逾期记录*/
    private int overdueMoreThanThreeMonth;
    /*征信报告要求为两周之内非空白简版征信报告*/
    private int nonBlankShortVersionCreditReport;
    /*简版征信贷记卡_贷款当前逾期且逾期金额大于等于500*/
    private int overdueMoreThanFiveHundred;
    /*简版征信贷款到期但仍显示正常还款*/
    private int normalRepayment;

    private int result;

}
