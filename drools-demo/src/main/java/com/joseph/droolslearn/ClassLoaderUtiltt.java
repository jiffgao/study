package com.joseph.droolslearn;

import cn.hutool.core.exceptions.UtilException;

import java.io.File;

public class ClassLoaderUtiltt {
    public static Class<?> loadClass(File jarOrDir, String name, ClassLoader classLoader) {
        try {
            ClassLoader classLoader1 = ClassLoaderUtiltt.class.getClassLoader();
            return getJarClassLoader(jarOrDir).loadClass(name);
        } catch (ClassNotFoundException e) {
            throw new UtilException(e);
        }
    }


    public static JarClassLoaderS getJarClassLoader(File jarOrDir) {
        return JarClassLoaderS.load(jarOrDir);
    }


}
