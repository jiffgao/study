package com.joseph.droolslearn;

import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.Message;
import org.kie.api.runtime.KieContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.StringReader;
import java.util.List;

/**
 * @Author: gcy
 * @createTime: 2023年05月08日 16:54:13
 * @version: 1.0
 * @Description:
 */
public class DroolsUtil {

    private Logger logger = LoggerFactory.getLogger(DroolsUtil.class);

    // KnowledgeBase 缓存(key：任务ID)
//    private static Map<Long, KieBase> ruleMap = new ConcurrentHashMap<>();

    private DroolsUtil() {
    }

    private static class SingletonHolder {
        static DroolsUtil instance = new DroolsUtil();
    }

    public static DroolsUtil getInstance() {
        return SingletonHolder.instance;
    }

    /**
     * 根据规则字符串重新编译规则，并将编译后的KieBase存入缓存
     * @param droolStrs
     * @param groupId
     * @return
     */
    public KieBase getDrlSession(final String droolStrs, final Long groupId) {

        try {
            KieServices kieServices = KieServices.Factory.get();
            KieFileSystem kfs = kieServices.newKieFileSystem();
            // for (String rule : droolStrs) {
                // 为防止规则文件名字重复，此处加上时间戳( 格式：任务ID+时间戳+.drl)
                String ruleFileName = groupId + System.currentTimeMillis() + ".drl";

                kfs.write("src/main/resources/rules/" + ruleFileName, kieServices.getResources().newReaderResource(new StringReader(droolStrs)));
//                kfs.write("src/main/resources/com/bjklb/drools/rules/" + ruleFileName, rule.getBytes("UTF-8"));
//             }
            KieBuilder kieBuilder = kieServices.newKieBuilder(kfs).buildAll();
            if (!kieBuilder.getResults().getMessages(Message.Level.ERROR).isEmpty()) {
                logger.error("规则引擎编译错误:{}", kieBuilder.getResults().getMessages());
                throw new RuntimeException("ExceptionType.RULE_IS_ERROR");
            }
            KieContainer kieContainer = kieServices.newKieContainer(kieServices.getRepository().getDefaultReleaseId());
            KieBase kBase = kieContainer.getKieBase();
            // 放入缓存
//                KieSession kieSession = kBase.newKieSession();
//                kieSession.addEventListener(new DebugRuleRuntimeEventListener());

//            ruleMap.put(groupId, kBase);
            return kBase;
        } catch (Exception e) {
            logger.error("规则引擎初始化失败，请查看错误信息:{}", e.getMessage());
            throw new RuntimeException("ExceptionType.RULE_IS_ERROR");
        }

    }

    /**
     * 根据场景获取缓存中的kbase,如果返回null,则表示缓存中没有
     * @param groupId
     * @return
     */
//     public KieBase getDrlBasesInCache(final Long groupId) {
//         try {
// //            KieBase kbase = ruleMap.get(groupId);
//             List<String> rules = RedisUtils.getCacheObject(CacheConstants.ENGINE_RULES_KEY + groupId);
//             if (rules == null) {
//                 return null;
//             }
//             KieBase kbase = getDrlSession(rules, groupId);
//             if (kbase == null) {
//                 return null;
//             } else {
//                 return kbase;
//             }
//         } catch (Exception e) {
//             logger.error("获取 KieBase 信息错误:{}", e);
//             throw new RuntimeException(ExceptionType.RULE_IS_ERROR);
//         }
//     }

    /**
     * 移除对应的规则
     * @param groupId
     */
//    public static void removeRuleMap(final Long groupId) {
//        ruleMap.remove(groupId);
//    }

    /**
     * 清空规则缓存
     */
//    public static void clearRuleMap() {
//        ruleMap.clear();
//    }
}
