package com.joseph.droolslearn;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.junit.jupiter.api.Test;
import org.kie.api.KieBase;
import org.kie.api.runtime.KieSession;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.net.MalformedURLException;
import java.util.List;

@SpringBootTest
public class AirRiskEntityTest1 {

    @Resource
    private KieBase kieBase;

    @Test
    public void testStatelessSession() throws MalformedURLException, ClassNotFoundException {
        KieSession ksession = kieBase.newKieSession();

        String str = "[\n" +
                "  {\n" +
                "    \"noLoan\": 0,\n" +
                "    \"noCreditCard\": 0,\n" +
                "    \"noValidCredit\": 0,\n" +
                "    \"assetAllocation\": 0,\n" +
                "    \"guarantorIndemnification\": 0,\n" +
                "    \"badDebt\": 0,\n" +
                "    \"overdueMoreThanTenMonth\": 0,\n" +
                "    \"overdueMoreThanThreeMonth\": 0,\n" +
                "    \"nonBlankShortVersionCreditReport\": 0,\n" +
                "    \"overdueMoreThanFiveHundred\": 0,\n" +
                "    \"normalRepayment\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"noLoan\": 0,\n" +
                "    \"noCreditCard\": 1,\n" +
                "    \"noValidCredit\": 0,\n" +
                "    \"assetAllocation\": 0,\n" +
                "    \"guarantorIndemnification\": 0,\n" +
                "    \"badDebt\": 0,\n" +
                "    \"overdueMoreThanTenMonth\": 0,\n" +
                "    \"overdueMoreThanThreeMonth\": 0,\n" +
                "    \"nonBlankShortVersionCreditReport\": 0,\n" +
                "    \"overdueMoreThanFiveHundred\": 0,\n" +
                "    \"normalRepayment\": 0\n" +
                "  }\n" +
                "]";


        List<AirRiskEntity> airRiskEntity = JSONArray.parseArray(str, AirRiskEntity.class);
        // ksession.insert(airRiskEntity);
        for (AirRiskEntity riskEntity : airRiskEntity) {
            ksession.insert(riskEntity);
        }
        String str2 = "[\n" +
                "  {\n" +
                "    \"score\": 1.2,\n" +
                "    \"grade\": \"\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"score\": 0.2,\n" +
                "    \"grade\": \"\"\n" +
                "  }\n" +
                "]";
        List<AiLakeEntity> aiLakeEntities = JSONArray.parseArray(str2, AiLakeEntity.class);
        for (AiLakeEntity aiLakeEntity : aiLakeEntities) {
        ksession.insert(aiLakeEntity);
        }
        ksession.fireAllRules();
        ksession.dispose();
        System.out.println(airRiskEntity);

    }
}
