package com.joseph.droolslearn;

import cn.hutool.core.util.ClassLoaderUtil;
import cn.hutool.extra.spring.*;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.joseph.SpringUtils;
import org.drools.compiler.kie.builder.impl.KieModuleKieProject;
import org.drools.compiler.kie.builder.impl.MemoryKieModule;
import org.junit.jupiter.api.Test;
import org.kie.api.KieBase;
import org.kie.api.runtime.KieSession;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.List;

// @SpringBootTest
public class AirRiskEntityTest {

    // @Resource
    // private KieBase kieBase;

    @Test
    public void testStatelessSession() throws MalformedURLException, ClassNotFoundException {
        // KieSession ksession = kieBase.newKieSession();

        // String str = "{\"noLoan\":1,\n" +
        //         "\"noCreditCard\":0,\n" +
        //         "\"noValidCredit\":0,\n" +
        //         "\"assetAllocation\":0,\n" +
        //         "\"guarantorIndemnification\":0,\n" +
        //         "\"badDebt\":0,\n" +
        //         "\"overdueMoreThanTenMonth\":0,\n" +
        //         "\"overdueMoreThanThreeMonth\":0,\n" +
        //         "\"nonBlankShortVersionCreditReport\":0,\n" +
        //         "\"overdueMoreThanFiveHundred\":0,\n" +
        //         "\"normalRepayment\":0}";
        // String str = "[\n" +
        //         "  {\n" +
        //         "    \"noLoan\": 0,\n" +
        //         "    \"noCreditCard\": 0,\n" +
        //         "    \"noValidCredit\": 0,\n" +
        //         "    \"assetAllocation\": 0,\n" +
        //         "    \"guarantorIndemnification\": 0,\n" +
        //         "    \"badDebt\": 0,\n" +
        //         "    \"overdueMoreThanTenMonth\": 0,\n" +
        //         "    \"overdueMoreThanThreeMonth\": 0,\n" +
        //         "    \"nonBlankShortVersionCreditReport\": 0,\n" +
        //         "    \"overdueMoreThanFiveHundred\": 0,\n" +
        //         "    \"normalRepayment\": 0\n" +
        //         "  },\n" +
        //         "  {\n" +
        //         "    \"noLoan\": 0,\n" +
        //         "    \"noCreditCard\": 1,\n" +
        //         "    \"noValidCredit\": 0,\n" +
        //         "    \"assetAllocation\": 0,\n" +
        //         "    \"guarantorIndemnification\": 0,\n" +
        //         "    \"badDebt\": 0,\n" +
        //         "    \"overdueMoreThanTenMonth\": 0,\n" +
        //         "    \"overdueMoreThanThreeMonth\": 0,\n" +
        //         "    \"nonBlankShortVersionCreditReport\": 0,\n" +
        //         "    \"overdueMoreThanFiveHundred\": 0,\n" +
        //         "    \"normalRepayment\": 0\n" +
        //         "  }\n" +
        //         "]";
        // KieModuleKieProject kieModuleKieProject = new KieModuleKieProject(new MemoryKieModule());
        // ClassLoader classLoader = kieModuleKieProject.getClassLoader();
        // Class<?> clazz = ClassLoaderUtiltt.loadClass(
        //         new File("D:\\Document\\WeChat Files\\gaojinfu003\\FileStorage\\File\\2023-06\\AiLakeEntity.jar"), "com.bjklb.engine.bean.AiLakeEntity",classLoader);

        URLClassLoader classLoader = new URLClassLoader(new URL[] { new URL("file:///E:/extlib/AiLakeEntity.jar") });
        Class<?> clazz = classLoader.loadClass("com.bjklb.engine.bean.AiLakeEntity");


        // System.out.println(kieModuleKieProject.getClassLoader());
        // Object data = JSONObject.parseObject("  {\n" +
        //         "    \"noLoan\": 0,\n" +
        //         "    \"noCreditCard\": 0,\n" +
        //         "    \"noValidCredit\": 0,\n" +
        //         "    \"assetAllocation\": 0,\n" +
        //         "    \"guarantorIndemnification\": 0,\n" +
        //         "    \"badDebt\": 0,\n" +
        //         "    \"overdueMoreThanTenMonth\": 0,\n" +
        //         "    \"overdueMoreThanThreeMonth\": 0,\n" +
        //         "    \"nonBlankShortVersionCreditReport\": 0,\n" +
        //         "    \"overdueMoreThanFiveHundred\": 0,\n" +
        //         "    \"normalRepayment\": 0\n" +
        //         "  }\n", clazz);
        Object data = JSONObject.parseObject("{\"modelId\":\"guest#9999#arbiter-10001#guest-9999#host-10001#model\",\"modelVersion\":\"202306120610533864520\",\"score\":0.0000000010550150867807607,\"timestamp\":1686550254441}", clazz);
        System.out.println(data);

        DroolsRuleEngineService droolsRuleEngineService = new DroolsRuleEngineService();
        droolsRuleEngineService.compileRuleAndexEcuteRuleEngine("package rules;\n" +
                "\n" +
                "import com.bjklb.engine.bean.AiLakeEntity;\n" +
                "\n" +
                "rule \"ailake01\"\n" +
                "    when\n" +
                "        $a:AiLakeEntity(score > 0 && score <= 0.2)\n" +
                "    then\n" +
                "        $a.setGrade(\"E\");\n" +
                "        System.out.println(\"fjdklsjakfl\");\n" +
                "        drools.halt();\n" +
                "end\n" +
                "\n" +
                "rule \"ailake02\"\n" +
                "    when\n" +
                "        $a:AiLakeEntity(score > 0.2 && score <= 0.4)\n" +
                "    then\n" +
                "        $a.setGrade(\"D\");\n" +
                "        drools.halt();\n" +
                "end\n" +
                "\n" +
                "rule \"ailake03\"\n" +
                "    when\n" +
                "        $a:AiLakeEntity(score > 0.4 && score <= 0.6)\n" +
                "    then\n" +
                "        $a.setGrade(\"C\");\n" +
                "        drools.halt();\n" +
                "end\n" +
                "\n" +
                "rule \"ailake04\"\n" +
                "    when\n" +
                "        $a:AiLakeEntity(score > 0.6 && score <= 0.8)\n" +
                "    then\n" +
                "        $a.setGrade(\"B\");\n" +
                "        drools.halt();\n" +
                "end\n" +
                "\n" +
                "rule \"ailake05\"\n" +
                "    when\n" +
                "        $a:AiLakeEntity(score > 0.8 && score <= 1.0)\n" +
                "    then\n" +
                "        $a.setGrade(\"A\");\n" +
                "        drools.halt();\n" +
                "end\n" +
                "\n" +
                "rule \"ailake06\"\n" +
                "    when\n" +
                "        $a:AiLakeEntity(score > 1.0)\n" +
                "    then\n" +
                "        $a.setGrade(\"S\");\n" +
                "        System.out.println(\"fjdklsjakfl\");\n" +
                "        drools.halt();\n" +
                "end\n",321L,data);

        // List<AirRiskEntity> airRiskEntity = JSONArray.parseArray(str, data.get);
            // ksession.insert(airRiskEntity);
        // for (AirRiskEntity riskEntity : airRiskEntity) {
        //     ksession.insert(data);
        // }
        // SpringUtils.getBean()
        // ksession.fireAllRules();
        // ksession.dispose();
        // System.out.println(airRiskEntity);

    }
}
