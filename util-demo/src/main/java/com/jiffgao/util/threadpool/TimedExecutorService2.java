package com.jiffgao.util.threadpool;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class TimedExecutorService2 extends ThreadPoolExecutor {

    long timeout;

    public TimedExecutorService2(int numThreads, long timeout, TimeUnit unit) {
        super(numThreads, numThreads, 0L, TimeUnit.MILLISECONDS,
                new ArrayBlockingQueue<Runnable>(1000));
        this.timeout = unit.toMillis(timeout);
    }

    @Override
    protected void beforeExecute(Thread thread, Runnable runnable) {
        Thread interruptionThread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    // Wait until timeout and interrupt this thread
                    Thread.sleep(timeout);
                    thread.interrupt();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        interruptionThread.start();
    }

    public static void main(String[] args) {
        int numThreads = 4, timeout = 5;
        ThreadPoolExecutor timedExecutor = new TimedExecutorService2(numThreads, timeout, TimeUnit.SECONDS);
        for (int i = 0; i < 6; i++) {
            timedExecutor.execute(new Business(i));
        }
        timedExecutor.shutdown();

    }
}