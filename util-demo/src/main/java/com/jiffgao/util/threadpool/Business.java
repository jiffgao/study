package com.jiffgao.util.threadpool;

public class Business implements Runnable {
    private int i;

    public Business(int i) {
        this.i = i;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + "\t" + i);
    }
}
