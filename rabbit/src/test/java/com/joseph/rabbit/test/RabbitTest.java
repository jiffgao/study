package com.joseph.rabbit.test;

import com.joseph.rabbit.RabbitApplicationTests;
import com.joseph.rabbit.entity.Student;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.AbstractJavaTypeMapper;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;

public class RabbitTest extends RabbitApplicationTests {

    @Autowired
    RabbitTemplate rabbitTemplate;


    @Test
    public void publishTest(){
        Student student = new Student(12,"zhangshan");

        rabbitTemplate.convertAndSend("myqueue",student);


        // MessageBody outMessage = new MessageBody();
        // outMessage.setMessageID(transNo);
        // outMessage.setMessageType(Constants.MESSAGE_TYPE_3+"_"+saveMsgNamePre);
        // outMessage.setContent(JdomUtil.toStringFmt(outStdDoc));
        // outMessage.setInsuFlag(insuFlag);
        // outMessage.setInsuCode(insuCode);
        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
        // rabbitTemplate.setExchange("exchange.topic."+insuFlag);
        // rabbitTemplate.setRoutingKey("rounting.processing."+insuFlag);
        rabbitTemplate.convertAndSend(student,(Message msg)->{
            MessageProperties messageProperties = msg.getMessageProperties();
            messageProperties.setDeliveryMode(MessageDeliveryMode.PERSISTENT);
            messageProperties.setHeader(AbstractJavaTypeMapper.DEFAULT_CONTENT_CLASSID_FIELD_NAME, Student.class);
            return msg;
        });
    }

    @Test
    public void fanoutTest(){
        Student student = new Student(12,"zhangshan");

        rabbitTemplate.convertAndSend("amq.fanout","hfjdks",student);
    }
}
