package com.joseph.rabbit.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Student implements Serializable {
    private int age;
    private String name;

    public Student(int age, String name) {
        this.age = age;
        this.name = name;
    }
}
