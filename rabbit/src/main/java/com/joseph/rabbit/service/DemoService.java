package com.joseph.rabbit.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.joseph.rabbit.entity.Student;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;

@Slf4j
@Component
public class DemoService {
    @Resource
    private ObjectMapper objectMapper;

    @RabbitListener(queues = "queue.topic.cic")
    public void consumer(Message message, Channel channel) throws IOException {
        MessageProperties messageProperties = message.getMessageProperties();
        // 消息ID
        long deliveryTag = messageProperties.getDeliveryTag();
        try {
            // log.info("消息：{}，开始处理", deliveryTag);

            byte[] messageBody = message.getBody();
            // log.info("存储报文-监听器监听消费消息-内容为：{} ", new String(messageBody));
            Student messageObject = objectMapper.readValue(messageBody, Student.class);
            log.info("student--{}", messageObject);
            // String messageID = messageObject.getMessageID();
            // String messageType = messageObject.getMessageType();
            // String content = messageObject.getContent();
            // String insuFlag = messageObject.getInsuFlag();
            // String insuCode = messageObject.getInsuCode();
            //
            // Boolean cacheFlag = redisService.setCacheObject2("ybt:mq:"+messageID, content, Constants.MQQUEUE_EXPIRATION, TimeUnit.SECONDS);
            // if(cacheFlag){
            //     AjaxResult ajaxResult = saveXmlService.saveXml(messageType, content, insuFlag, insuCode);
            //     log.info("处理报文信息结果---->"+ajaxResult.toString());
            //     Integer rcode = (Integer) ajaxResult.get("code");
            //     if(rcode == 500){
            //         //重回队列
            //         Thread.sleep(Constants.MQQUEUE_EXPIRATION*1000);
            //         //报文处理失败，可进行下次消费
            //         log.info("消息[{}]-{}，处理报文失败，等待下次消费", deliveryTag, messageID);
            //         channel.basicNack(deliveryTag, false, true);
            //     }
            //     if(rcode == 501){
            //         //直接转死信队列
            //         log.info("消息丢弃，将变为死信队列消息，消息ID：{} ", deliveryTag);
            //         channel.basicNack(deliveryTag, false, false);
            //     }
            //     if(rcode != 500 && rcode != 501){
            //         //第一个参数为：消息的分发标识(唯一);第二个参数：是否允许批量确认消费
            channel.basicAck(deliveryTag, false);
            // log.info("消息[{}]，消费成功", deliveryTag);
            //     }
            // }else{
            //     Thread.sleep(Constants.MQQUEUE_EXPIRATION*1000);
            //     //该消息正在被其他消费者消费，直接拒绝
            //     log.info("正在处理消息[{}]-{}中，短时间内不进行重复消费", deliveryTag, messageID);
            //     channel.basicNack(deliveryTag, false, true);
            // }

        } catch (Exception e) {
            log.error("消息[{}]-存储报文-监听器监听消费消息-发生异常：", deliveryTag, e.fillInStackTrace());

            // 消费失败，将 requeue属性设置为true，即消费失败的消息重回队列。
            // channel.basicNack(deliveryTag, false, true);
        }
    }

    @RabbitListener(queues = "dead.queue.common")
    public void deadconsumer(Message message, Channel channel) throws IOException {
        MessageProperties messageProperties = message.getMessageProperties();
        // 消息ID
        long deliveryTag = messageProperties.getDeliveryTag();
        try {
            // log.info("dead 消息：{}，开始处理", deliveryTag);

            byte[] messageBody = message.getBody();
            // log.info("dead 存储报文-监听器监听消费消息-内容为：{} ", new String(messageBody));
            Student messageObject = objectMapper.readValue(messageBody, Student.class);
            log.info("dead student--{}", messageObject);
            channel.basicAck(deliveryTag, false);
            // log.info("消息[{}]，消费成功", deliveryTag);

        } catch (Exception e) {
            log.error("dead消息[{}]-存储报文-监听器监听消费消息-发生异常：", deliveryTag, e.fillInStackTrace());

        }
    }

}
