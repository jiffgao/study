package com.joseph.rabbit.config;

import com.joseph.rabbit.mqmodel.RabbitModuleInfo;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @Author:
 * @createTime: 2023年02月16日 17:01:41
 * @version:
 * @Description:
 */
@Configuration
@ConfigurationProperties(prefix = "spring.rabbitmq")
@Data
public class RabbitModuleConfig {
    private List<RabbitModuleInfo> modules;
}
