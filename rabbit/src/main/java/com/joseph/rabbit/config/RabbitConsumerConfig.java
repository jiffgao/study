package com.joseph.rabbit.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author:
 * @createTime: 2023年02月16日 17:01:41
 * @version:
 * @Description:
 */
@Configuration
@ConfigurationProperties(prefix = "spring.rabbitmq.listener")
@Data
public class RabbitConsumerConfig {
    private String[] queuenames;

    @Bean
    public String[] queueNames() {
        return queuenames;
    }
}
