package com.jiffgao;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class PrintABC {
    ReentrantLock lock = new ReentrantLock();
    Condition conditionA = lock.newCondition();
    Condition conditionB = lock.newCondition();
    Condition conditionC = lock.newCondition();
    private int value;

    public static void main(String[] args) {
        new PrintABC().printABC();
    }

    private void printABC() {
        new Thread(new ThreadA()).start();
        new Thread(new ThreadB()).start();
        new Thread(new ThreadC()).start();
    }

    class ThreadA implements Runnable {

        @Override
        public void run() {
            System.out.println("ThreadA start");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            lock.lock();
            System.out.println("ThreadA lock");
            try {
                while (value % 3 != 0)
                    conditionA.await();
                System.out.print("A");
                value++;
                conditionB.signal();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
                System.out.println("ThreadA unlock");
            }
        }
    }

    class ThreadB implements Runnable {

        @Override
        public void run() {
            System.out.println("ThreadB start");
            lock.lock();
            System.out.println("ThreadB lock");
            try {
                while (value % 3 != 1)
                    conditionB.await();
                System.out.print("B");
                value++;
                conditionC.signal();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
                System.out.println("ThreadB unlock");
            }
        }
    }

    class ThreadC implements Runnable {

        @Override
        public void run() {
            System.out.println("ThreadC start");
            lock.lock();
            System.out.println("ThreadC lock");
            try {
                while (value % 3 != 2)
                    conditionC.await();
                System.out.println("C");
                value = 0;
                // conditionC.signal();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
                System.out.println("ThreadC unlock");
            }
        }
    }
}
