package com.jiffgao;

import java.util.concurrent.Exchanger;

public class ExchangerTest {
    public static void main(String[] args) {
        Exchanger<String> exchanger = new Exchanger<>();
        Thread t1 = new Thread(() -> {
            String s = "T1";
            try {
                s = exchanger.exchange(s);
                System.out.println("t1 " + s);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "t1");
        Thread t2 = new Thread(() -> {
            String s = "T2";
            try {
                s = exchanger.exchange(s);
                System.out.println("t2 " + s);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "t2");
        t1.start();
        t2.start();

    }
}
