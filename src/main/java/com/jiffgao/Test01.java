package com.jiffgao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Test01 {
    volatile static List<Object> list = Collections.synchronizedList(new ArrayList<>());

    public static void main(String[] args) {
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                list.add(new Object());
                System.out.println(list.size());
            }
        }, "t1").start();
        new Thread(() -> {
            boolean flag = true;
            while (flag) {
                if (list.size() == 5) {
                    System.out.println("t2 end");
                    flag = false;
                }
            }
        }, "t2").start();

        ExecutorService executorService = Executors.newSingleThreadExecutor();
    }
}
