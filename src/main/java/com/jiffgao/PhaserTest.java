package com.jiffgao;

import java.util.concurrent.Phaser;

public class PhaserTest {
    static MarriagePhase phase = new MarriagePhase();

    static class Person implements Runnable {
        String name;

        public Person(String name) {
            this.name = name;
        }

        public void arrive() throws InterruptedException {
            Thread.sleep(1000L);
            System.out.printf("%s 到达现场！\n", name);
            phase.arriveAndAwaitAdvance();
        }

        public void eat() throws InterruptedException {
            Thread.sleep(1000L);
            System.out.printf("%s 吃完！\n", name);
            phase.arriveAndAwaitAdvance();
        }

        public void leave() throws InterruptedException {
            Thread.sleep(1000L);
            System.out.printf("%s 离开！\n", name);
            phase.arriveAndAwaitAdvance();
        }

        public void hug() throws InterruptedException {
            if (name.equals("新郎") || name.equals("新娘")) {
                Thread.sleep(1000L);
                System.out.printf("%s 洞房！\n", name);
                phase.arriveAndAwaitAdvance();
            } else {
                phase.arriveAndDeregister();
            }
        }

        @Override
        public void run() {
            try {
                arrive();
                eat();
                leave();
                hug();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    static class MarriagePhase extends Phaser {
        @Override
        protected boolean onAdvance(int phase, int registeredParties) {
            switch (phase) {
                case 0:
                    System.out.println("所有人都到齐了！" + registeredParties);
                    System.out.println();
                    return false;
                case 1:
                    System.out.println("所有人都吃完了！" + registeredParties);
                    System.out.println();
                    return false;
                case 2:
                    System.out.println("所有人都离开了！" + registeredParties);
                    System.out.println();
                    return false;
                case 3:
                    System.out.println("婚礼结束，抱抱" + registeredParties);
                    System.out.println();
                    return true;
                default:
                    return true;
            }
        }
    }

    public static void main(String[] args) {
        phase.bulkRegister(7);
        for (int i = 0; i < 5; i++) {
            final int nameIndex = i;
            new Thread(new Person("P" + i)).start();
        }

        new Thread(new Person("新郎")).start();
        new Thread(new Person("新娘")).start();
    }
}
