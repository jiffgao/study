package com.jiffgao.lambda.demo1;

public class LambdaTest {

    public static void main(String[] args) {

        Factory factory = User::new;

        System.out.println(factory.getObject());
        factory = () -> {
            return new User("haha",1);
        };
        System.out.println(factory.getObject());
    }

}
