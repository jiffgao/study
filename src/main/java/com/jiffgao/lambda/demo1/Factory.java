package com.jiffgao.lambda.demo1;

public interface Factory {
    Object getObject();
}
