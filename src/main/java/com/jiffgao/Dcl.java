package com.jiffgao;

public class Dcl {
    private volatile Dcl intstance = null;
    private Dcl() {

    }
    public Dcl getIntstance() {
        if (intstance == null) {
            synchronized (this) {
                if (intstance == null) {
                    intstance = new Dcl();
                }
            }
        }
        return intstance;
    }
}
