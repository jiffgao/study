package com.jiffgao;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ReentrantLockTest {
    Lock lock = new ReentrantLock();

    void m1() {
        int i = 0;
        try {
            lock.lock();
            for (int j = 0; j < 10; j++) {
                System.out.println(i++);
            }
        } finally {
            lock.unlock();
        }

    }

    void m2() {
        lock.lock();
        System.out.println("m2.......");
        lock.unlock();
    }

    public static void main(String[] args) {
        ReentrantLockTest test = new ReentrantLockTest();
        new Thread(test::m1).start();
        new Thread(test::m2).start();
    }
}
