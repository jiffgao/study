package com.jiffgao;

import org.apache.flink.api.common.RuntimeExecutionMode;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * flink实时数据处理
 */
public class StreamBatchModeWordCount {
    public static void main(String[] args) throws Exception {
        // 1. 准备flink 实时处理环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setRuntimeMode(RuntimeExecutionMode.BATCH);

        // 2. 实时读取数据
        DataStreamSource<String> stringDataStreamSource = env.readTextFile("E:\\_javaproject\\learn\\study\\flink-learn\\words.txt");

        // 3.单词切分
        SingleOutputStreamOperator<Tuple2<String, Long>> tuple2SingleOutputStreamOperator
                = stringDataStreamSource.flatMap((FlatMapFunction<String, Tuple2<String, Long>>) (s, collector) -> {
            String[] s1 = s.split(" ");
            for (String s2 : s1) {
                collector.collect(Tuple2.of(s2, 1L));
            }
        }).returns(Types.TUPLE(Types.STRING, Types.LONG));

        // 分组统计wordcount结果
        tuple2SingleOutputStreamOperator.keyBy(tp -> tp.f0).sum(1).print();

        // 流式计算需要执行execute
        env.execute();
    }
}
