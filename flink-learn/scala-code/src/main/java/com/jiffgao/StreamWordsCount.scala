package com.jiffgao

import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment

object StreamWordsCount {

  def main(args: Array[String]): Unit = {
    //1 创建环境
    val environment = StreamExecutionEnvironment.getExecutionEnvironment

    // 导入隐式转换
    import org.apache.flink.streaming.api.scala._

    // 读取文件
    val ds = environment.readTextFile("E:\\_javaproject\\learn\\study\\flink-learn\\words.txt")

    // 统计
    ds.flatMap(line=>{line.split(" ")})
      .map((_,1))
      .keyBy(_._1)
      .sum(1)
      .print()

    environment.execute()
  }

}
