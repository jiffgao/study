package com.joseph.sharding.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.joseph.sharding.bean.Customer;
import com.joseph.sharding.mapper.CustomerMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class Testcontroller {

    CustomerMapper customerMapper;

    public Testcontroller(CustomerMapper customerMapper) {
        this.customerMapper = customerMapper;
    }

    @GetMapping("/test")
    public void test() {
        // List<Customer> customers = customerMapper.selectList();
        // System.out.println(customers.size());
        // System.out.println(customers);
        Customer customer = new Customer();
        customer.setCardNo("340721000000000000");
        // List<String> customers = customerMapper.selectPolicyNo(customer);
        QueryWrapper<Customer> customerQueryWrapper = new QueryWrapper<>();
        customerQueryWrapper.lambda().eq(Customer::getCardNo, "340721000000000000");
        List<Customer> customers1 = customerMapper.selectList(customerQueryWrapper);
        System.out.println(customers1.size());
        System.out.println(customers1);
    }
}
