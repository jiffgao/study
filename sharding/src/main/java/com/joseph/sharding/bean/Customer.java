package com.joseph.sharding.bean;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 个人信息表
 * </p>
 *
 * @author [mybatis plus generator]
 * @since 2022-11-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("ybt_customer")
public class Customer implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 姓名
     */
    private String name;

    /**
     * 性别
     */
    private String sex;

    /**
     * 生日
     */
    private Date birthday;

    /**
     * 证件类型
     */
    private String cardType;

    /**
     * 证件号
     */
    private String cardNo;

    /**
     * 证件有效起始日期
     */
    private Date cardStartDate;

    /**
     * 证件有效截止日期
     */
    private Date cardEndDate;

    /**
     * 长期有效标识
     */
    private String permanentFlag;

    /**
     * 国籍
     */
    private String nationality;

    /**
     * 职业类型
     */
    private String occType;

    /**
     * 职业代码
     */
    private String occCode;

    /**
     * 年收入
     */
    private BigDecimal annualIncome;

    /**
     * 家庭年收入
     */
    private BigDecimal householdIncome;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新人
     */
    private String updateBy;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 租户Id
     */
    private Long tenantId;


}
