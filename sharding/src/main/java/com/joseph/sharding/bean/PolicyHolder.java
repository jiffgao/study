package com.joseph.sharding.bean;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 投保人信息表
 * </p>
 *
 * @author [mybatis plus generator]
 * @since 2022-11-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class PolicyHolder implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 保单号
     */
    private String policyNo;

    /**
     * 投保单号
     */
    private String ciNo;

    /**
     * 地址信息表主键
     */
    private Long addressId;

    /**
     * 客户主键
     */
    private Long customerId;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新人
     */
    private String updateBy;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 租户Id
     */
    private Long tenantId;


}
