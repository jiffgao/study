package com.joseph.sharding.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.joseph.sharding.bean.Customer;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface CustomerMapper extends BaseMapper<Customer> {


    // @Select("select * from ybt_customer")
    // List<Customer> selectList();

    @Select("select p.policy_no from ybt_customer c,ybt_policy_holder p where c.id = p.customer_id and c.card_no = #{cardNo}")
    List<String> selectPolicyNo(Customer customer);
}

