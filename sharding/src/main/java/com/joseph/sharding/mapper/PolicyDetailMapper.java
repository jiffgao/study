package com.joseph.sharding.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.joseph.sharding.bean.PolicyDetail;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 保单险种表 Mapper 接口
 * </p>
 *
 * @author [mybatis plus generator]
 * @since 2022-11-21
 */
@Mapper
public interface PolicyDetailMapper extends BaseMapper<PolicyDetail> {

    @Select("select main_product_code ,count(1)  from ybt_policy_detail group by main_product_code")
    List<Map<String,Integer>> queryProduct();
}
