package com.joseph.sharding;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.joseph.sharding.bean.Customer;
import com.joseph.sharding.bean.Orders;
import com.joseph.sharding.mapper.CustomerMapper;
import com.joseph.sharding.mapper.OrdersMapper;
import com.joseph.sharding.mapper.PolicyDetailMapper;
import com.joseph.sharding.mapper.PolicyMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;

@SpringBootTest
class ShardingApplicationTests {

    @Test
    void contextLoads() {
    }


    @Autowired
    private OrdersMapper ordersMapper;

    @Test
    public void addOrdersDB() {
        for (int i = 1; i <= 10; i++) {
            Orders orders = new Orders();
            orders.setId(i);
            orders.setCustomerId(new Random().nextInt(10));
            orders.setOrderType(i);
            orders.setAmount(1000.0 * i);
            ordersMapper.insert(orders);
        }
    }

    @Test
    public void queryOrdersDB() {
        Orders orders = new Orders();
        // orders.setCustomerId(5);
        orders.setId(7);
        List<Orders> orders1 = ordersMapper.selectList(orders);
        System.out.println(orders1);
    }

    @Autowired
    CustomerMapper customerMapper;

    @Test
    public void queryCustomers() {
        // List<Customer> customers = customerMapper.selectList();
        // System.out.println(customers.size());
        // System.out.println(customers);
    }

    @Test
    public void queryPolicyNos() {
        Customer customer = new Customer();
        customer.setCardNo("340721000000000000");
        // List<String> customers = customerMapper.selectPolicyNo(customer);
        QueryWrapper<Customer> customerQueryWrapper = new QueryWrapper<>();
        customerQueryWrapper.lambda().eq(Customer::getCardNo, "340721000000000000");
        List<Customer> customers1 = customerMapper.selectList(customerQueryWrapper);
        System.out.println(customers1.size());
        System.out.println(customers1);
    }

    @Autowired
    PolicyMapper policyMapper;

    @Test
    public void queryPolicy() throws ParseException {
        List<Map<String, Long>> stringIntegerMap = policyMapper.queryPlocy();
        System.out.println("stringIntegerMap.size()==" + stringIntegerMap.size());
        System.out.println(stringIntegerMap);

        // 按日期分组统计数据
        Map<String, Long> result = new TreeMap<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = null;
        Date endDate = null;
        for (Map<String, Long> data : stringIntegerMap) {
            String dateStr = String.valueOf(data.get("days"));
            Long value = data.get("num");
            try {
                Date date = sdf.parse(dateStr);
                if (startDate == null || date.before(startDate)) {
                    startDate = date;
                }
                if (endDate == null || date.after(endDate)) {
                    endDate = date;
                }
                result.put(dateStr, value);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(sdf.parse("2023-07-01"));
        while (calendar.getTime().compareTo(sdf.parse("2023-07-24")) <= 0) {
            String key = sdf.format(calendar.getTime());
            if (!result.containsKey(key)) {
                result.put(key, 0L);
            }
            calendar.add(Calendar.DATE, 1);
        }
        System.out.println("result==" + result);
    }

    @Autowired
    PolicyDetailMapper policyDetailMapper;

    @Test
    public void queryProduct()   {
        List<Map<String, Integer>> maps = policyDetailMapper.queryProduct();

        System.out.println(maps);
    }

}
