package com.joseph.k8s;

import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.apis.CoreV1Api;
import io.kubernetes.client.openapi.models.V1Pod;
import io.kubernetes.client.util.ClientBuilder;
import io.kubernetes.client.util.Yaml;
import io.kubernetes.client.util.credentials.AccessTokenAuthentication;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class K8sApplicationTests {

    @Test
    void contextLoads() {
    }

    public static void main(String[] args) throws ApiException {
        ApiClient apiClient = new ClientBuilder()
                .setBasePath("https://192.168.241.21:6443")
                .setVerifyingSsl(false)
                .setAuthentication(new AccessTokenAuthentication("eyJhbGciOiJSUzI1NiIsImtpZCI6IjZZVXdheUFoMkd1MFREb1ZtZUx0WVZVNHNkZVlNT1F6S1lQOTN5YzVrUFUifQ.eyJhdWQiOlsidW5rbm93biJdLCJleHAiOjQ4MTE5NjkwOTIsImlhdCI6MTY4OTkwNTA5MiwiaXNzIjoicmtlIiwia3ViZXJuZXRlcy5pbyI6eyJuYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsInNlcnZpY2VhY2NvdW50Ijp7Im5hbWUiOiJkaG9yc2UtYWRtaW4iLCJ1aWQiOiJhMTBmYTM5Ny1iYzA0LTQyMDItYTRhMC0yMmQxYzdhOWNhOWUifX0sIm5iZiI6MTY4OTkwNTA5Miwic3ViIjoic3lzdGVtOnNlcnZpY2VhY2NvdW50Omt1YmUtc3lzdGVtOmRob3JzZS1hZG1pbiJ9.4Aco_V-bzl_mgjxG_BfVDgVt483C9-SRTjvWxW7LsrxrIKLJdwdngMEGSDVZF7MBXwbRQrQcYAvr42Q2tZXG8s1OiqBnMKVyTmY6IGOW50QZVOdXTOowY-N4X59HqqGnBYI8AJa3LoKBC7M7cgyvrr5vUej8IbefcCKhiRimlBX3OyxMUhgu49YONt8xzZsF3_1ykDBBxKj8RsywD92ZdlodFTgNcBSoAP80rgO8On0VAmVX_3wbMmQdSEW3ouC0I738jqkkLJDAUfuCM4E2Poq4ckVCQK4I48Myz8WbTULISJKhADMqc-bFQxhf43fk3tgOTHhY2p9Z63bHpXQRoA"))
                .build();
        apiClient.setConnectTimeout(1000);
        apiClient.setReadTimeout(1000);
        CoreV1Api coreApi = new CoreV1Api(apiClient);
        V1Pod v1Pod = coreApi.readNamespacedPod("hello-1-qa-dhorse-b679b65d6-nqtcw", "default", "");
        String podYaml = Yaml.dump(v1Pod);
        System.out.println(podYaml);
    }


}
