object Main {
  println("Hello world! up")

  def main(args: Array[String]): Unit = {
    println("Hello world!")
    val range = 1 until(10, 1)
    for (elem <- range) {
      println(elem)
    }

    // 定义函数
    // 签名 (Int, Int) => Int  （参数列表）=> 返回值类型
    // 匿名函数 (a: Int, b: Int) => {
    //      a + b
    //    }
    val function: (Int, Int) => Int = (a: Int, b: Int) => {
      a + b
    }

    println(function(2, 3))
  }

  println("Hello world! down")
}