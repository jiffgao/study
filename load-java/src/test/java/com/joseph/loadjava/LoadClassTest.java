package com.joseph.loadjava;

import cn.hutool.core.util.ClassLoaderUtil;
import com.alibaba.fastjson.JSONObject;
import org.junit.jupiter.api.Test;

import java.io.File;

public class LoadClassTest extends LoadJavaApplicationTests {

    @Test
    public void test() throws ClassNotFoundException {

        Class<?> clazz = ClassLoaderUtil.loadClass(new File("E:\\_javaproject\\airisk-ms\\airisk-engine\\target\\classes"), "com.bjklb.engine.bean.AirRiskEntity");

        System.out.println(Class.forName("com.bjklb.engine.bean.AirRiskEntity",false,ClassLoaderUtil.getClassLoader()));
        Object data = JSONObject.parseObject("  {\n" +
                "    \"noLoan\": 0,\n" +
                "    \"noCreditCard\": 0,\n" +
                "    \"noValidCredit\": 0,\n" +
                "    \"assetAllocation\": 0,\n" +
                "    \"guarantorIndemnification\": 0,\n" +
                "    \"badDebt\": 0,\n" +
                "    \"overdueMoreThanTenMonth\": 0,\n" +
                "    \"overdueMoreThanThreeMonth\": 0,\n" +
                "    \"nonBlankShortVersionCreditReport\": 0,\n" +
                "    \"overdueMoreThanFiveHundred\": 0,\n" +
                "    \"normalRepayment\": 0\n" +
                "  }\n", clazz);
        System.out.println(data);
    }
}
