package com.joseph.loadjava.controller;

import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @PostMapping("/test")
    public String test(@RequestBody JSONObject jsonObject) throws Exception {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        Class<?> cla;
        try {
            cla = Class.forName("com.joseph.loadjava.bean.Student");
            // cla = classLoader.loadClass("com.joseph.loadjava.bean.Student");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        Object data = JSONObject.parseObject(jsonObject.getJSONObject("data").toJSONString(), cla);

        return data.toString();
    }
}
