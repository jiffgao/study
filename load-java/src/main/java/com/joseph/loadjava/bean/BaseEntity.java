package com.joseph.loadjava.bean;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 0-通过
 * 1-拒绝
 * 2-转人工
 */
@Data
@Accessors(chain = true)
public class BaseEntity implements Serializable {

    /*处理结果*/
    private int result;
}
