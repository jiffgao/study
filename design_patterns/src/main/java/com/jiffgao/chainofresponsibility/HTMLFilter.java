package com.jiffgao.chainofresponsibility;

public class HTMLFilter implements Filter {
    public void doFilter(Msg msg){
        String msg1 = msg.getMsg();
        msg1 = msg1.replace("<","[");
        msg1 = msg1.replace(">","]");
        msg.setMsg(msg1);
    }


}
