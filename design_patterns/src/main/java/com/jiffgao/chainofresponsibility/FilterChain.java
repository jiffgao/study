package com.jiffgao.chainofresponsibility;

import java.util.ArrayList;
import java.util.List;

public class FilterChain {
    List<Filter> filterList = new ArrayList<>();

    public FilterChain add(Filter filter) {
        filterList.add(filter);
        return this;
    }

    public void doFilter(Msg msg){
        for (Filter filter : filterList) {
            filter.doFilter(msg);
        }
    }
}
