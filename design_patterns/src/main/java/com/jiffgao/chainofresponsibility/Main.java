package com.jiffgao.chainofresponsibility;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Msg msg = new Msg();
        msg.setMsg("dajiahao:<script>,996");
        FilterChain filterChain = new FilterChain();
        filterChain.add(new HTMLFilter());
        filterChain.doFilter(msg);
        System.out.println(msg);
    }


}

class Msg {
    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "Msg{" +
            "msg='" + msg + '\'' +
            '}';
    }
}
