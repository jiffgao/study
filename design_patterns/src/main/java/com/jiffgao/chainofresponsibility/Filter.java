package com.jiffgao.chainofresponsibility;

public interface Filter {
    void doFilter(Msg msg);
}
