package com.jiffgao.abstractfactory;

public class Car extends Vehicle{

    @Override
    void go() {
        System.out.println("this is car");
    }
}
