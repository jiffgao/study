package com.jiffgao.abstractfactory;

public abstract class Vehicle {
    abstract void go();
}
