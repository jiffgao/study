package com.jiffgao.abstractfactory;

public abstract class AbstractFactory {
    abstract Weapon createWeapon();
    abstract Food createFood();
    abstract Vehicle createVehicle();
}
