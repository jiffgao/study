package com.jiffgao.abstractfactory;

public abstract class Food {
    abstract void printFoodName();
}
