package com.jiffgao.abstractfactory;

public class Bread extends Food{
    @Override
    void printFoodName() {
        System.out.println("this is bread");
    }
}
