package com.jiffgao.abstractfactory;

public abstract class Weapon {
    abstract void shoot();
}
