package com.jiffgao.thread;

public class VolatileFlag {
private static volatile boolean running = true;
public static void main(String[] args) throws InterruptedException {
    new Thread(() -> {
        long i = 0L;
        while (running) {
            i++;
        }
        System.out.println("i===" + i);// 1664192880 1815870167
    }).start();

    Thread.sleep(1000);
    running = false;
}
}
