package com.jiffgao.thread;

public class Main implements Runnable {
    @Override
    public void run() {
        System.out.println("hello world");
    }

    public static void main(String[] args) {
        // new Main().run();
        // new Thread(new Main()).start();
        Main main1 = new Main();
        new Thread(() -> main1.set("zhangshan", 100.00)).start();
        new Thread(() -> System.out.println(main1.getBalance("zhangshan"))).start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(main1.balance);
    }

    String name;
    double balance;

    private synchronized void set(String name, double balance) {
        this.name = name;
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.balance = balance;
    }

    private double getBalance(String name) {
        return balance;
    }
}
