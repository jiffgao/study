package com.jiffgao.thread;

public class Interrupt {
    public static void main(String[] args) throws InterruptedException {
Thread end = new Thread(() -> {
    while (!Thread.interrupted()) {
    }
    System.out.println("end");
});
end.start();
Thread.sleep(1000L);
end.interrupt();
    }
}
