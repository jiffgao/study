package com.jiffgao;

import com.jiffgao.factory.Cat;
import com.jiffgao.util.Sorter;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Cat[] a = {new Cat(3), new Cat(9), new Cat(2), new Cat(5)};
        // new Sorter<Cat>().sort(a, new Comparator<Cat>() {
        //     @Override
        //     public int compare(Cat o1, Cat o2) {// 此处可以根据需求任意指定比较器
        //         if (o1.getWeight() < o2.getWeight())
        //             return -1;
        //         else if (o1.getWeight() > o2.getWeight())
        //             return 1;
        //         else
        //             return 0;
        //     }
        // });
        new Sorter<Cat>().sort(a, (o1, o2) -> {
            if (o1.getWeight() < o2.getWeight())
                return -1;
            else if (o1.getWeight() > o2.getWeight())
                return 1;
            else
                return 0;
        });
        System.out.println(Arrays.asList(a));
    }
}
