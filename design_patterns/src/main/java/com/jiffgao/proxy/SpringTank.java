package com.jiffgao.proxy;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;
import java.util.Random;

public class SpringTank {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("application.xml");
        Tank2 tank = (Tank2) classPathXmlApplicationContext.getBean("tank2");
        tank.move();
    }
}

class Tank2 {
    public void move() {
        System.out.println("tank is moving");
        try {
            Thread.sleep(new Random().nextInt(10000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

@Aspect
class TimeProxy {
    @Before("execution(void com.jiffgao.proxy.Tank2.move())")
    public void before() {
        System.out.println("method start...32132");
    }

    @After("execution(void com.jiffgao.proxy.Tank2.move())")
    public void after() {
        System.out.println("mothod end...32132");
    }
}