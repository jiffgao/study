package com.jiffgao.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Random;

public final class Tank implements Moveable {

    @Override
    public void move() {
        System.out.println("tank is moving");
        try {
            Thread.sleep(new Random().nextInt(10000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Tank tank = new Tank();
        // System.getProperties().put("jdk.proxy.ProxyGenerator.saveGeneratedFiles","true");
        System.getProperties().put("sun.misc.ProxyGenerator.saveGeneratedFiles","true");
        Moveable m = (Moveable) Proxy.newProxyInstance(Tank.class.getClassLoader(),// 类加载器
            new Class[]{Moveable.class},// 必须传被代理对象所实现的接口，如果没有实现接口，jdk代理无法实现
            new MyinvocationHandler(tank));
        m.move();
    }
}
/**
 * @Description
 * @Params
 * @Return
 * @Time 2021/8/14 17:03
 * @Author JiffGao
 */
class MyinvocationHandler implements InvocationHandler{

    private final Tank tank;

    public MyinvocationHandler(Tank tank) {
        this.tank = tank;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("method:" + method.getName());
        Object invoke = method.invoke(tank, args);
        System.out.println("method:" + method.getName() + "end");
        return invoke;
    }
}

interface Moveable {
    void move();
}