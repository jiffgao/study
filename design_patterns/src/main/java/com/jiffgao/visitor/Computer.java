package com.jiffgao.visitor;

public class Computer {
    private ComputerPart cpu = new CPU();
    private ComputerPart board = new Board();
    private ComputerPart memory = new Memory();
    public void accept(Visitor visitor){
        this.cpu.accept(visitor);
        this.board.accept(visitor);
        this.memory.accept(visitor);
    }
    public static void main(String[] args) {
        PersonOneVisitor personOneVisitor = new PersonOneVisitor();
        new Computer().accept(personOneVisitor);
        System.out.println(personOneVisitor.totalPrice);
    }
}

abstract class ComputerPart{
    abstract void accept(Visitor visitor);
    abstract double getPrice();

}

interface Visitor{
    void visitCpu(ComputerPart cpu);
    void visitBoard(ComputerPart board);
    void visitMemory(ComputerPart memory);
}
class CPU extends ComputerPart{

    @Override
    void accept(Visitor visitor) {
        visitor.visitCpu(this);
    }

    @Override
    double getPrice() {
        return 2000;
    }
}
class Board extends ComputerPart{

    @Override
    void accept(Visitor visitor) {
        visitor.visitCpu(this);
    }

    @Override
    double getPrice() {
        return 700;
    }
}
class Memory extends ComputerPart{

    @Override
    void accept(Visitor visitor) {
        visitor.visitCpu(this);
    }

    @Override
    double getPrice() {
        return 300;
    }
}

class PersonOneVisitor implements Visitor{
    double totalPrice;
    @Override
    public void visitCpu(ComputerPart cpu) {
        totalPrice += cpu.getPrice()*0.7;
    }

    @Override
    public void visitBoard(ComputerPart board) {
        totalPrice += board.getPrice()*0.8;
    }

    @Override
    public void visitMemory(ComputerPart memory) {
        totalPrice += memory.getPrice()*0.9;
    }
}
