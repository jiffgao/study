package com.jiffgao;

public class Singleton {
    private static final Singleton INSTENCE;
    static {
        INSTENCE = new Singleton();
    }
    private Singleton() {
    }
    public static Singleton getInstence() {
        return INSTENCE;
    }
    public static void main(String[] args) {
        new Thread(() -> System.out.println(Singleton.getInstence())).start();
        new Thread(() -> System.out.println(Singleton.getInstence())).start();
        new Thread(() -> System.out.println(Singleton.getInstence())).start();
    }
}
