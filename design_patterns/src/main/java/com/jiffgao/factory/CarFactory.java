package com.jiffgao.factory;

public class CarFactory {
    private CarFactory INSTANCE = new CarFactory();

    public CarFactory getInstance() {
        return INSTANCE;
    }

    private CarFactory() {
        this.INSTANCE = INSTANCE;
    }

    public Moveable create() {
        System.out.println("create a car");
        return new Car();
    }
}
