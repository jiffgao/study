package com.jiffgao.factory;

public interface Moveable {
    void go();
}
