package com.jiffgao.factory;

public class Cat {
    private int weight;

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Cat{" +
            "weight=" + weight +
            '}';
    }

    public Cat(int weight) {
        this.weight = weight;
    }
}
