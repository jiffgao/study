package com.joseph;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class Main {

    public static void main(String[] args) throws IOException, InterruptedException, URISyntaxException {

    }

    @Test
    public void mkdires() throws URISyntaxException, IOException, InterruptedException {
        URI uri = new URI("hdfs://192.168.241.151:8020");

        Configuration configuration = new Configuration();

        FileSystem fileSystem = FileSystem.get(uri, configuration, "root");


        fileSystem.mkdirs(new Path("/hahaha"));

        fileSystem.close();
    }

    @Test
    public void testCopyFromFile() throws URISyntaxException, IOException, InterruptedException {
        URI uri = new URI("hdfs://192.168.241.151:8020");

        Configuration configuration = new Configuration();

        FileSystem fileSystem = FileSystem.get(uri, configuration, "root");


        fileSystem.copyFromLocalFile(new Path("C:\\Users\\10550\\Desktop\\020012023032201.txt"), new Path("/hahaha"));

        fileSystem.close();
    }

    /**
     *
     * @throws IOException
     * @throws URISyntaxException
     * @throws InterruptedException
     */
    @Test
    public void testCopyToLocalFile() throws IOException, URISyntaxException, InterruptedException {
        URI uri = new URI("hdfs://192.168.241.151:8020");

        Configuration configuration = new Configuration();

        FileSystem fileSystem = FileSystem.get(uri, configuration, "root");


        fileSystem.copyToLocalFile(new Path("/hahaha/020012023032201.txt"),
                new Path("C:\\Users\\10550\\Desktop\\test"));

        fileSystem.close();
    }

}