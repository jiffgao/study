package com.joseph.sharding.config;

import com.alibaba.druid.util.StringUtils;
import com.baomidou.dynamic.datasource.DynamicRoutingDataSource;
import com.baomidou.dynamic.datasource.provider.AbstractJdbcDataSourceProvider;
import com.baomidou.dynamic.datasource.provider.DynamicDataSourceProvider;
import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DataSourceProperty;
import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DynamicDataSourceAutoConfiguration;
import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DynamicDataSourceProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.shardingsphere.api.config.sharding.ShardingRuleConfiguration;
import org.apache.shardingsphere.api.config.sharding.TableRuleConfiguration;
import org.apache.shardingsphere.shardingjdbc.api.ShardingDataSourceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.jdbc.support.JdbcUtils;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

@Configuration
@Slf4j
@AutoConfigureBefore({DynamicDataSourceAutoConfiguration.class, SpringBootConfiguration.class})
public class DatasourceConfig {
    @Autowired
    AbstractApplicationContext context;

    @Autowired
    private DynamicDataSourceProperties properties;

    @Lazy
    @Resource
    private DataSource shardingDataSource;

    @Autowired
    SourceProperties sourceProperties;

    @Bean
    public DynamicDataSourceProvider dynamicDataSourceProvider() {
        // Map<String, DataSourceProperty> datasourceMap = properties.getDatasource();
        // return new AbstractDataSourceProvider() {
        //     @SneakyThrows
        //     @Override
        //     public Map<String, DataSource> loadDataSources() {
        //         Map<String, DataSource> dataSourceMap = createDataSourceMap(datasourceMap);
        //         // 重写shardingDataSource 将数据库中查询出来的数据源塞到shardingDataSource中
        //         dataSourceMap.put("sharding-data-source", getDataSource());
        //         return dataSourceMap;
        //     }
        // };

        return new AbstractJdbcDataSourceProvider(sourceProperties.getDriverClassName(), sourceProperties.getUrl(), sourceProperties.getUsername(), sourceProperties.getPassword()) {
            @Override
            public Map<String, DataSource> loadDataSources() {
                Connection conn = null;
                Statement stmt = null;
                try {
                    // 由于 SPI 的支持，现在已无需显示加载驱动了
                    // 但在用户显示配置的情况下，进行主动加载
                    if (!StringUtils.isEmpty(sourceProperties.getDriverClassName())) {
                        Class.forName(sourceProperties.getDriverClassName());
                        log.info("成功加载数据库驱动程序");
                    }
                    conn = DriverManager.getConnection(sourceProperties.getUrl(), sourceProperties.getUsername(), sourceProperties.getPassword());
                    log.info("成功获取数据库连接");
                    stmt = conn.createStatement();
                    Map<String, DataSourceProperty> dataSourcePropertiesMap = executeStmt(stmt);

                    Map<String, DataSource> dataSourceMap = createDataSourceMap(new HashMap<>());
                    dataSourceMap.put("sharding-data-source", getDataSource(dataSourcePropertiesMap));
                    return dataSourceMap;
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    JdbcUtils.closeConnection(conn);
                    JdbcUtils.closeStatement(stmt);
                }
                return null;
            }

            @Override
            protected Map<String, DataSourceProperty> executeStmt(Statement statement) throws SQLException {
                ResultSet rs = statement.executeQuery("select s.slave, s.username, s.password, s.url_prepend, s.url_append, s.driver_class_name from te_source s where s.status = 0 and s.del_flag = 0");
                Map<String, DataSourceProperty> map = new HashMap<>();
                while (rs.next()) {
                    String name = rs.getString(Details.SLAVE.getCode());
                    if ("master".equals(name) || "slave".equals(name)) {
                        continue;
                    }
                    String username = rs.getString(Details.USERNAME.getCode());
                    String password = rs.getString(Details.PASSWORD.getCode());
                    String urlPrepend = rs.getString(Details.URL_PREPEND.getCode());
                    String url = urlPrepend.concat(rs.getString(Details.URL_APPEND.getCode()));
                    String driver = rs.getString(Details.DRIVER_CLASS_NAME.getCode());
                    DataSourceProperty property = new DataSourceProperty();
                    if (urlPrepend.lastIndexOf("/") > -1) {
                        property.setPoolName(urlPrepend.substring(urlPrepend.lastIndexOf("/") > -1 ? (urlPrepend.lastIndexOf("/") + 1) : 0));
                    }
                    property.setUsername(username);
                    property.setPassword(password);
                    property.setUrl(url);
                    property.setDriverClassName(driver);
                    map.put(name, property);
                }
                return map;
            }
        };
    }

    public DataSource getDataSource(Map<String, DataSourceProperty> dataSourcePropertiesMap) throws SQLException {
        ShardingRuleConfiguration shardingRuleConfig = new ShardingRuleConfiguration();
        shardingRuleConfig.getTableRuleConfigs().add(getYbtCustomerTableRuleConfiguration(dataSourcePropertiesMap.size()));
        shardingRuleConfig.getTableRuleConfigs().add(getYbtPolicyhTableRuleConfiguration(dataSourcePropertiesMap.size()));
        // shardingRuleConfig.getBindingTableGroups().add("t_order, t_order_item");
        // shardingRuleConfig.getBroadcastTables().add("t_address");
        // shardingRuleConfig.setDefaultDatabaseShardingStrategyConfig(new InlineShardingStrategyConfiguration("user_id", "demo_ds_${user_id % 2}"));
        // shardingRuleConfig.setDefaultTableShardingStrategyConfig(new StandardShardingStrategyConfiguration("order_id", new PreciseModuloShardingTableAlgorithm()));
        Properties properties1 = new Properties();
        properties1.setProperty("sql.show","true");
        return ShardingDataSourceFactory.createDataSource(createDataSourceMap(dataSourcePropertiesMap), shardingRuleConfig, properties1);
    }

    private static TableRuleConfiguration getYbtCustomerTableRuleConfiguration(int size) {
        return new TableRuleConfiguration("ybt_customer", "ds_${0.." + (size - 1) + "}.ybt_customer");
    }
    private static TableRuleConfiguration getYbtPolicyhTableRuleConfiguration(int size) {
        return new TableRuleConfiguration("ybt_policy", "ds_${0.." + (size - 1) + "}.ybt_policy");
    }


    private Map<String, DataSource> createDataSourceMap(Map<String, DataSourceProperty> dataSourcePropertiesMap) {
        Map<String, DataSource> result = new HashMap<>(2);
        AtomicInteger i = new AtomicInteger();
        dataSourcePropertiesMap.forEach(
                (name, property) -> {
                    log.info("name:{}, property:{}", name, property);
                    result.put("ds_" + i.get(), createDataSource(property));
                    i.getAndIncrement();
                });
        return result;
    }

    private DataSource createDataSource(final DataSourceProperty sourceProperty) {
        BasicDataSource result = new BasicDataSource();
        result.setDriverClassName(sourceProperty.getDriverClassName());
        result.setUrl(sourceProperty.getUrl());
        result.setUsername(sourceProperty.getUsername());
        result.setPassword(sourceProperty.getPassword());
        return result;
    }

    public static enum Details {
        SLAVE("slave", "数据源编码"),
        USERNAME("username", "用户名"),
        PASSWORD("password", "密码"),
        URL_PREPEND("url_prepend", "连接地址"),
        URL_APPEND("url_append", "连接参数"),
        DRIVER_CLASS_NAME("driver_class_name", "驱动");

        private final String code;
        private final String info;

        private Details(String code, String info) {
            this.code = code;
            this.info = info;
        }

        public String getCode() {
            return this.code;
        }

        public String getInfo() {
            return this.info;
        }
    }

    @Primary
    @Bean
    public DataSource dataSource() {
        DynamicRoutingDataSource dataSource = new DynamicRoutingDataSource();
        dataSource.setStrict(properties.getStrict());
        dataSource.setStrategy(properties.getStrategy());
        dataSource.setP6spy(properties.getP6spy());
        dataSource.setSeata(properties.getSeata());
        String primary = "true".equals(this.context.getEnvironment().getProperty("spring.shardingsphere.enabled"))
                ? "sharding-data-source" : properties.getPrimary();
        dataSource.setPrimary(primary);
        log.info("Mrssz Datasource primary: {}", primary);
        return dataSource;
    }
}
