package com.joseph.sharding.bean;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 保单表
 * </p>
 *
 * @author [mybatis plus generator]
 * @since 2022-12-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("ybt_policy")
public class Policy implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 保单号
     */
    private String policyNo;

    /**
     * 投保单号
     */
    private String ciNo;

    /**
     * 单证号
     */
    private String sdNo;

    /**
     * 银行代码
     */
    private String bankCode;

    /**
     * 分行代码
     */
    private String subBankCode;

    /**
     * 网点代码
     */
    private String nodeBankCode;

    /**
     * 银行操作员
     */
    private String bankOperator;

    /**
     * 保司代码
     */
    private String insuCode;

    /**
     * 保司分公司代码
     */
    private String subInsuCode;

    /**
     * 保险公司联系方式
     */
    private String insuPhone;

    /**
     * 保险公司地址
     */
    private String insuAddress;

    /**
     * 机构代码
     */
    private String deptCode;

    /**
     * 机构名称
     */
    private String deptName;

    /**
     * 机构地址
     */
    private String deptAddress;

    /**
     * 保司代理人编码

     */
    private String agentCode;

    /**
     * 保司代理人名称
     */
    private String agentName;

    /**
     * 保司代理机构编码
     */
    private String agencyCode;

    /**
     * 保司代理机构名称

     */
    private String agencyName;

    /**
     * 销售渠道
     */
    private String saleChannel;

    /**
     * 交费账户名
     */
    private String accountName;

    /**
     * 银行卡号
     */
    private String accountNo;

    /**
     * 签单日期
     */
    private Date signDate;

    /**
     * 保单状态
     */
    private String policyState;

    /**
     * 投保日期
     */
    private Date applyDate;

    /**
     * 保单生效日期
     */
    private Date effDate;

    /**
     * 保单终止日期
     */
    private Date terminationDate;

    /**
     * 保费
     */
    private BigDecimal premium;

    /**
     * 保额
     */
    private BigDecimal insuredAmount;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新人
     */
    private String updateBy;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 租户Id
     */
    private Long tenantId;


}
