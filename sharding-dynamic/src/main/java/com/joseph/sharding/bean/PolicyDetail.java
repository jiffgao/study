package com.joseph.sharding.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 保单险种表
 * </p>
 *
 * @author [mybatis plus generator]
 * @since 2022-11-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("ybt_policy_detail")
public class PolicyDetail implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 保单号
     */
    private String policyNo;

    /**
     * 投保单号
     */
    private String ciNo;

    /**
     * 单证号
     */
    private String sdNo;

    /**
     * 产品代码
     */
    private String productCode;

    /**
     * 产品主代码
     */
    private String mainProductCode;

    /**
     * 保费
     */
    private BigDecimal premium;

    /**
     * 保额
     */
    private BigDecimal insuredAmount;

    /**
     * 份数
     */
    private Integer share;

    /**
     * 交费频率
     */
    private String paymentFrequency;

    /**
     * 交费期间类型
     */
    private String paymentPeriodType;

    /**
     * 交费期间
     */
    private Byte paymentPeriod;

    /**
     * 保险期间类型
     */
    private String insuPeriodType;

    /**
     * 保险期间
     */
    private Byte insuPeriod;

    /**
     * 续保标志
     */
    private String renewalType;

    /**
     * 领取年期
     */
    private Byte receivePeriod;

    /**
     * 领取年期类型
     */
    private String receivePeriodType;

    /**
     * 领取方式
     */
    private String receiveType;

    /**
     * 领取年龄
     */
    private Byte receiveStartAge;

    /**
     * 领取年期
     */
    private Byte receiveYear;

    /**
     * 保单生效日期
     */
    private Date effDate;

    /**
     * 保单终止日期
     */
    private Date terminationDate;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新人
     */
    private String updateBy;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 租户Id
     */
    private Long tenantId;


}
