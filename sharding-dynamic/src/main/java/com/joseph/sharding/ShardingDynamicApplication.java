package com.joseph.sharding;

import org.apache.shardingsphere.shardingjdbc.spring.boot.SpringBootConfiguration;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {SpringBootConfiguration.class})
@MapperScan(value = {"com.joseph.sharding.mapper"})
public class ShardingDynamicApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShardingDynamicApplication.class, args);
    }

}
