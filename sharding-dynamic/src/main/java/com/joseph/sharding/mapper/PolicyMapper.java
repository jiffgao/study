package com.joseph.sharding.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.joseph.sharding.bean.Policy;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 保单表 Mapper 接口
 * </p>
 *
 * @author [mybatis plus generator]
 * @since 2022-11-21
 */
@Mapper
public interface PolicyMapper extends BaseMapper<Policy> {

    // @Select("SELECT \n" +
    //         "\tt1.days,\n" +
    //         "\tifnull(t2.num,0) num\n" +
    //         "FROM (\n" +
    //         "WITH RECURSIVE DATE_LIST (days) AS (      \t\n" +
    //         "SELECT DATE('2023-07-24')  AS DATES   \t\n" +
    //         "UNION ALL       \t\n" +
    //         "SELECT DATE_ADD(days, INTERVAL -1 DAY) \t\n" +
    //         "FROM DATE_LIST WHERE DATE_ADD(days, INTERVAL -1 DAY) > '2023-07-01'  \n" +
    //         ") \n" +
    //         "SELECT  days FROM DATE_LIST ) t1 \n" +
    //         "left join (\n" +
    //         "select sign_date days,count(1) num from ybt_policy\n" +
    //         "  GROUP BY sign_date ) t2\n" +
    //         "on t1.days=t2.days")
    @Select("select sign_date days,count(1) num from ybt_policy where sign_date between '2023-07-01' and '2023-07-24' " +
            "  GROUP BY sign_date ")
    public List<Map<String,Long>> queryPlocy();

}
