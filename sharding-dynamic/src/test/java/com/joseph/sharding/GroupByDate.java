package com.joseph.sharding;

import java.text.SimpleDateFormat;
import java.util.*;

public class GroupByDate {
    public static void main(String[] args) {
        //输入数据
        List<Data> dataList = new ArrayList<>();
        dataList.add(new Data("2023-07-22", 5));
        dataList.add(new Data("2023-07-23", 10));
        dataList.add(new Data("2023-07-25", 15));
        dataList.add(new Data("2023-07-26", 20));

        //按日期分组统计数据
        Map<String, Integer> result = new TreeMap<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = null;
        Date endDate = null;
        for (Data data : dataList) {
            String dateStr = data.getDate();
            int value = data.getValue();
            try {
                Date date = sdf.parse(dateStr);
                if (startDate == null || date.before(startDate)) {
                    startDate = date;
                }
                if (endDate == null || date.after(endDate)) {
                    endDate = date;
                }
                result.put(dateStr, value);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //确保日期连续，并对于没有数据的日期设置为0
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        while (calendar.getTime().compareTo(endDate) <= 0) {
            String key = sdf.format(calendar.getTime());
            if (!result.containsKey(key)) {
                result.put(key, 0);
            }
            calendar.add(Calendar.DATE, 1);
        }

        //打印结果
        for (String key : result.keySet()) {
            System.out.println(key + ": " + result.get(key));
        }
    }

    static class Data {
        private String date;
        private int value;

        public Data(String date, int value) {
            this.date = date;
            this.value = value;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }
    }
}