package com.joseph.controller;

import com.joseph.compiler.CompilerUtils;
import com.joseph.compiler.CreateJarUtils;
import org.apache.commons.io.FileUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;

@RestController
public class TestController {

    // static String basePath = "E:\\_javaproject\\learn\\study\\compiler";
    static String basePath = "/app/compiler";
    // 生成jar文件路径
    static String jarFilePath = basePath+"/target";
    // 需要编译的源文件路径
    static String[] srcFiles = {
            "/src/main/java/com/joseph/domain/"
            //     ,
            // "/rmi/com/demo/wms/tasks/rmi/",
            // "/rmi/com/demo/wms/tasks/rmi/po/",
            // "/rmi/com/demo/wms/tasks/rmi/client/"
    };
    static String jarReyOnPath = basePath+"/src/main/resources/lib";
    static String jarFileName = "rim";
    static String encoding = "utf-8";

    @GetMapping("/test")
    public String test() {
        String sourcePath = "";
        String classPath = "";
        try {
            // 将RMI需要使用的JAVA文件拷贝到制定目录中
            System.out.println("分隔符:" + File.separator);
            System.out.println("资源拷贝......");
            sourcePath = jarFilePath + File.separator + "source";
            copySource(sourcePath);// 拷贝资源
            System.out.println("资源拷贝结束");
            System.out.println("编译资源......");
            // 编译java文件
            classPath = jarFilePath + File.separator + "class";
            try {
                CompilerUtils.compiler(sourcePath, classPath, basePath, encoding, jarReyOnPath);
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println("编译资源结束");
            System.out.println("生成jar......");
            // 生成jar文件
            String tempJar = CreateJarUtils.createTempJar(classPath, jarFilePath, jarFileName);
            System.out.println("生成jar完成");
            // 删除临时文件
            ExeSuccess(sourcePath, classPath);
            return tempJar;
        } catch (IOException e) {
            e.printStackTrace();
            deleteTempFile(sourcePath, classPath);

        } finally {
        }
        return "生成jar失败";
    }

    private static void ExeSuccess(String sourcePath, String classPath) {
        final String sourcedir = sourcePath;
        final String classdir = classPath;
        // 程序结束后，通过以下代码删除生成的文件
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                deleteTempFile(sourcedir, classdir);
                System.out.println("***************执行完毕**********************");
            }
        });
    }

    private static void deleteTempFile(String sourcePath, String classPath) {
        // 程序结束后，通过以下代码删除生成的class 和java文件
        try {
            File sourceFile = new File(sourcePath);
            if (sourceFile.exists()) {
                FileUtils.deleteDirectory(sourceFile);
            }
            File classFile = new File(classPath);
            if (classFile.exists()) {
                FileUtils.deleteDirectory(classFile);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private static void copySource(String sourcePath) throws IOException {
        for (String f : srcFiles) {
            String path = f.replace("/", File.separator);
            System.out.println(path);
            File srcFile = new File(basePath + path);
            File targetFile = new File(sourcePath + path);
            FileUtils.copyDirectory(srcFile, targetFile, new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    System.out.println(pathname);
                    return pathname.getName().endsWith(".java");
                }
            });
        }
    }
}
