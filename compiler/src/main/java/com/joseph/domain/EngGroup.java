package com.joseph.domain;

import java.io.Serializable;

/**
 * <p>
 * 规则组表
 * </p>
 *
 * @author [mybatis plus generator]
 * @since 2023-04-20
 */
public class EngGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private Long id;

    /**
     * 规则组编号/名称
     */
    private String engGroupCode;

    /**
     * eng_rule主键id
     */
    private Long engRuleId;

    /**
     * 规则是否删除，（默认0-未删除），1-已删除
     */
    private Integer delFlag;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEngGroupCode() {
        return engGroupCode;
    }

    public void setEngGroupCode(String engGroupCode) {
        this.engGroupCode = engGroupCode;
    }

    public Long getEngRuleId() {
        return engRuleId;
    }

    public void setEngRuleId(Long engRuleId) {
        this.engRuleId = engRuleId;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public String toString() {
        return "EngGroup{" +
                "id=" + id +
                ", engGroupCode='" + engGroupCode + '\'' +
                ", engRuleId=" + engRuleId +
                ", delFlag=" + delFlag +
                '}';
    }
}
