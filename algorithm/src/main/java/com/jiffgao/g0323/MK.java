package com.jiffgao.g0323;

import java.util.HashMap;
import java.util.HashSet;

public class MK {

    /**
     * 查找数组中出现次数为k的数
     * 一个数组只有一个数字出现了k次，其他的数字都是m次
     * @param arr
     */
    private static int test4(int[] arr, int k, int m) {
        int[] ints = new int[32];
        for (int i : arr) {
            for (int j = 0; j < ints.length; j++) {
                // 查看第j为上是不是1，如果是1，则辅助数组ints[j]加1
                // if ((i & (1 >> j)) != 0) {
                //     ints[j]++;
                // }
                ints[j] += ((i >> j) & 1);
            }
        }
        int result = 0;
        for (int i = 0; i < ints.length; i++) {
            // 如果第i位上有1，则使用或运算赋值给结果
            if (ints[i] % m != 0) {
                result |= (1 << i);
            }
        }
        return result;
    }

    /**
     * 经典解
     * 查找数组中出现次数为k的数
     * 一个数组只有一个数字出现了k次，其他的数字都是m次
     * @param arr
     */
    private static int test5(int[] arr, int k, int m) {
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int num : arr) {
            if (map.containsKey(num)) {
                map.put(num, map.get(num) + 1);
            } else {
                map.put(num, 1);
            }
        }
        for (int num : map.keySet()) {
            if (map.get(num) == k) {
                return num;
            }
        }
        return -1;
    }

    /**
     * 对数器
     * @param range
     * @param k
     * @param m
     * @return
     */
    private static int[] randomArray(int kinds, int range, int k, int m) {
        // 生成出现k次的数
        int valueK = randomNumber(range);
        int[] arr = new int[k + m * (kinds - 1)];
        int i = 0;
        for (; i < k; i++) {
            arr[i] = valueK;
        }
        kinds--;
        HashSet<Integer> set = new HashSet<>();
        set.add(valueK);
        while (kinds != 0) {
            int valueM;
            do {
                // 生成出现m次的数
                valueM = randomNumber(range);
                // 如果已存在重新生成
            } while (set.contains(valueM));
            // 加入set
            set.add(valueM);
            // 填充至集合
            for (int j = 0; j < m; j++) {
                arr[i] = valueM;
                i++;
            }
            // 数值种类减1
            kinds--;
        }

        // 打乱数组顺序
        for (int j = 0; j < arr.length; j++) {
            int index = (int) (Math.random() * arr.length);
            int temp = arr[j];
            arr[j] = arr[index];
            arr[index] = temp;
        }

        return arr;
    }

    /**
     * 生成随机数
     * @param range
     * @return
     */
    private static int randomNumber(int range) {
        return ((int) (Math.random() * range) + 1) - ((int) (Math.random() * range) + 1);
    }

    public static void main(String[] args) {
        int range = 1000;
        int kinds = 100;
        int max = 100;
        int testTime = 10;

        for (int i = 0; i < testTime; i++) {
            int a = (int) (Math.random() * max) + 1;
            int b = (int) (Math.random() * max) + 1;
            int k = Math.min(a, b);
            int m = Math.max(a, b);
            if (k == m) {
                m++;
            }
            int[] arr = randomArray(kinds, range, k, m);
            if (test4(arr, k, m) != test5(arr, k, m)) {
                System.out.println("error");
                for (int j : arr) {
                    System.out.print(j + ", ");
                }
            }
        }
    }
}
