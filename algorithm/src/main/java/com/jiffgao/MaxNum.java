package com.jiffgao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MaxNum {
    public static void main(String[] args) throws IOException {
        // BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        // String line = bufferedReader.readLine();
        // String[] split = line.split(",");
        System.out.println("40".compareTo("4"));
        int[] ints = new int[]{2, 20, 23, 4, 8};
        System.out.println(getMaxNum(ints));
        // bufferedReader.close();
    }

    /**
     * @Description
     * 1、先按首位进行排序
     * @Params [nums]
     * @Return java.lang.String
     * @Time 2021/8/13 16:28
     * @Author JiffGao
     */
    private static String getMaxNum(int[] nums) {
        StringBuilder maxNum = new StringBuilder();
        ArrayList<String> strings = new ArrayList<>();
        for (int anInt : nums) {
            strings.add(String.valueOf(anInt));
        }
        strings.sort((o1, o2) -> (o2 + o1).compareTo(o1 + o2));
        System.out.println(strings);
        if (strings.get(0).equals("0"))
            return "0";
        for (String string : strings) {
            maxNum.append(string);
        }

        return maxNum.toString();
    }
}
