package com.jiffgao;

/**
 * 打印一个数字的32位二进制信息
 */
public class Test1 {
    public static void main(String[] args) {
        // int num = 1;
        // prin(num);
        int a = 678;
        int b = 7932;
        a = a ^ b;
        b = a ^ b;
        a = a ^ b;
        System.out.println("a= " + a);
        System.out.println("b= " + b);
    }

    public static void prin(int num) {
        for (int i = 31; i >= 0; i--) {
            System.out.print((num & (1 << i)) == 0 ? "0" : "1");
        }
    }
}
