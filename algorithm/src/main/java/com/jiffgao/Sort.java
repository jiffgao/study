package com.jiffgao;

import java.util.Arrays;

public class Sort {
    public static void main(String[] args) {
        Integer[] arr = new Integer[]{12, 4, 9, 6, 10, 2, 6, 78, 8, 3};
        // selectSort(arr);
        // popSort(arr);
        insertSort(arr);
        System.out.println(Arrays.asList(arr));
    }

    /**
     * @Description 选择排序
     * @Params [arr]
     * @Return void
     * @Time 2021/8/13 14:36
     * @Author JiffGao
     */
    public static void selectSort(Integer[] arr) {
        int temp;
        int minIndex;
        for (int i = 0; i < arr.length - 1; i++) {
            minIndex = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[minIndex] > arr[j]) {
                    minIndex = j;
                }
            }
            if (i != minIndex) {
                temp = arr[i];
                arr[i] = arr[minIndex];
                arr[minIndex] = temp;
            }
        }
    }

    /**
     * @Description 冒泡排序
     * @Params [arr]
     * @Return void
     * @Time 2021/8/13 15:39
     * @Author JiffGao
     */
    public static void popSort(Integer[] arr) {
        int temp;
        boolean flag;
        for (int i = 1; i < arr.length; i--) {
            flag = true;
            for (int j = 0; j < arr.length - i; j++) {
                if (arr[j] > arr[j + 1]) {
                    temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                    flag = false;
                }
            }
            if (flag) {
                break;
            }
        }
    }

    /**
     * @Description 插入排序
     * @Params [arr]
     * @Return void
     * @Time 2021/8/13 15:39
     * @Author JiffGao
     */
    public static void insertSort(Integer[] arr) {
        int temp;
        int index;
        for (int i = 1; i < arr.length; i++) {
            temp = arr[i];
            index = i;
            for (int j = i; j >= 0; j--) {
                if (arr[j] > temp) {
                    arr[j + 1] = arr[j];
                    index = j;
                }
            }
            if (i != index)
                arr[index] = temp;
        }
    }


}