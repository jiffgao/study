package com.jiffgao.niuke;

import java.util.Scanner;

/**
 * @Author:
 * @createTime: 2023年04月08日 22:30:32
 * @version:
 * @Description:
 */
public class ab1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int i = in.nextInt();
        MyStack myStack = new MyStack(i);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextLine()) { // 注意 while 处理多个 case
            String next = in.nextLine();
            if (next.startsWith("push")) {
                myStack.push(Integer.parseInt(next.substring(next.indexOf(" ") + 1)));
            } else if (next.startsWith("top")) {
                System.out.println(myStack.top());
            } else if (next.startsWith("pop")) {
                System.out.println(myStack.pop());
            }
        }
    }
}

class MyStack {
    int[] data;
    // 当前栈顶指针
    int top = 0;
    int dataSize;
    int size = 0 ;//栈中元素个数
    public MyStack(int size) {
        dataSize = size;
        data = new int[size];
    }

    public void push(int num) {
        // if (top + 1 == dataSize) {
        //     System.out.println("error");
        //     data[top] = num;
        // } else {
        //     data[top] = num;
        //     top++;
        // }
        if(this.size == this.dataSize) {//元素的个数已经达到栈的最大容量，不允许存储，报错
            System.out.println("error") ;
        } else {
            data[top++] = num ;//在栈顶指针的位置增加新元素，栈顶指针更新+1
            this.size++ ;//栈中元素个数更新+1
        }
    }

    public String pop() {
        if (top == 0) {
            return "error";
        }
        return String.valueOf(data[--top]);
    }

    public String top() {
        if (top == 0) {
            return "error";
        }
        return String.valueOf(data[top-1]);
    }
}