package com.jiffgao.niuke;

import java.util.HashMap;
import java.util.Stack;

/**
 * @Author:
 * @createTime: 2023年04月04日 20:26:57
 * @version:
 * @Description:
 */
public class ReverseList {
    static class ListNode {
        int val;
        ListNode next = null;

        ListNode(int val) {
            this.val = val;
        }

        @Override
        public String toString() {
            // return "ListNode{" +
            //         "val=" + val +
            //         ", next=" + next +
            //         '}';
            return val +
                    ", " + next;
        }
    }

    // 使用栈解决
    public ListNode ReverseList(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        Stack<ListNode> listNodes = new Stack<>();
        while (head != null) {
            listNodes.push(head);
            head = head.next;
        }
        // 创建一个新列表，弹出一个元素
        ListNode node = listNodes.pop();
        // 记录head
        ListNode temp = node;
        while (listNodes.size() != 0) {
            // 新链表指向弹出来的元素
            node.next = listNodes.pop();
            // 来到尾结点
            node = node.next;
        }
        node.next = null;
        return temp;
    }

    // 双链表
    public ListNode ReverseList2(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode newHead = null;

        while (head != null){
            // 记录原链表读取位置
            ListNode temp = head.next;
            // 将head指向新链表
            head.next = newHead;
            // 更新新链表
            newHead = head;
            // 恢复原链表读取位置
            head = temp;
        }
        return newHead;
    }


    public static void main(String[] args) {
        ListNode listNode = new ListNode(1);
        ListNode listNode1 = new ListNode(2);
        ListNode listNode2 = new ListNode(3);
        ListNode listNode3 = new ListNode(4);
        listNode.next = listNode1;
        listNode1.next = listNode2;
        listNode2.next = listNode3;
        System.out.println(listNode);

        ListNode listNode4 = new ReverseList().ReverseList2(listNode);
        System.out.println(listNode4);
    }
}
